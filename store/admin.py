from django.contrib import admin
from .models import *
from solo.admin import SingletonModelAdmin


admin.site.register(Item)
admin.site.register(Collection)
admin.site.register(News)
admin.site.register(Help)
admin.site.register(Footer)
admin.site.register(Advantages)
admin.site.register(Slider)
admin.site.register(PublicOffer)
admin.site.register(FooterInfo)
admin.site.register(PhotoColor)
admin.site.register(About, SingletonModelAdmin)
admin.site.register(HelpPhoto, SingletonModelAdmin)

