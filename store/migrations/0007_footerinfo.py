# Generated by Django 4.0.4 on 2022-04-19 07:45

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('store', '0006_remove_about_photo_about_photo1_about_photo2_and_more'),
    ]

    operations = [
        migrations.CreateModel(
            name='FooterInfo',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('contact_type', models.CharField(choices=[('phone', 'Phone Number'), ('email', 'E-mail'), ('instagram', 'Instagram'), ('telegram', 'Telegram'), ('whatsapp', 'Whatsapp')], max_length=100)),
            ],
        ),
    ]
