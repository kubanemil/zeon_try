from django.db import models
import json
from ckeditor.fields import RichTextField
from colorfield.fields import ColorField
from django.contrib.auth.models import User
from django.core.exceptions import ValidationError
from solo.models import SingletonModel
# from phonenumber_field.modelfields import PhoneNumberField
# Create your models here.

class PhotoColor(models.Model):
    photo = models.ImageField(upload_to='static/images/item',)
    color = ColorField()
    def __str__(self):
        return str(self.photo) + " " + str(self.color)

class Collection(models.Model):
    photo1 = models.ImageField(upload_to='static/images/collection', ) #can be added up to 8 images
    name = models.CharField(max_length=100, default="Unknown Collection")
    def add_item(self):
        pass
    def __str__(self):
        return self.name


class Item(models.Model):
    name = models.CharField(max_length=100)
    item_number = models.CharField(max_length=200)
    photo1 = models.ForeignKey(PhotoColor, related_name='pc1', on_delete=models.CASCADE) #can be added up to 8 images
    photo2 = models.ForeignKey(PhotoColor, related_name='pc2', on_delete=models.CASCADE)
    photo3 = models.ForeignKey(PhotoColor, related_name='pc3', on_delete=models.CASCADE)
    photo4 = models.ForeignKey(PhotoColor, related_name='pc4', on_delete=models.CASCADE)
    photo5 = models.ForeignKey(PhotoColor, related_name='pc5', on_delete=models.CASCADE)
    photo6 = models.ForeignKey(PhotoColor, related_name='pc6', on_delete=models.CASCADE)
    photo7 = models.ForeignKey(PhotoColor, related_name='pc7', on_delete=models.CASCADE)
    photo8 = models.ForeignKey(PhotoColor, related_name='pc8', on_delete=models.CASCADE)
    collection = models.ForeignKey(Collection, related_name="items", on_delete=models.CASCADE, default=Collection.objects.first())
    # color1 = models.ForeignKey(PhotoColor, on_delete=models.SET_NULL, null=True) #create color choice as html max is 8 colors
    color2 = ColorField(default='#FFB1FB')
    color3 = ColorField(default='#38FF13')
    color4 = ColorField(default='#FFC621')
    color5 = ColorField(default='#FFF5D7')
    color6 = ColorField(default='#C5FFFC')
    color7 = ColorField(default='#4F8C27')
    color8 = ColorField(default='#A13219')
    price = models.IntegerField(default=0)
    old_price = models.IntegerField(default=0)
    description = RichTextField() #ckeditor
    min_size = models.IntegerField(default=10)
    max_size = models.IntegerField(default=20)
    tissue_components = models.CharField(max_length=1000, default='cloth')
    amount = models.IntegerField(default=1)
    material = models.CharField(max_length=1000, default='cloth')
    is_popular = models.BooleanField(default=False)
    is_new = models.BooleanField(default=True)
    is_favorite = models.BooleanField(default=False)

    @property
    def discount(self):
        return round((1 - (self.price/self.old_price)), 2)
    
    def __str__(self):
        return self.name

class News(models.Model):
    photo = models.ImageField(upload_to='static/images/news')
    title = models.CharField(max_length=50)
    description = RichTextField() #ckeditor
    def __str__(self):
        return self.title
    
class About(SingletonModel):
    photo1 = models.FileField(upload_to='static/images/about', )
    photo2 = models.FileField(upload_to='static/images/about', null=True, blank=True)
    photo3 = models.FileField(upload_to='static/images/about',  null=True, blank=True)
    title = models.CharField(max_length=50)
    description = RichTextField() #ckeditor
    def __str__(self):
        return self.title


class HelpPhoto(SingletonModel):
    photo = models.ImageField()
    def __str__(self):
        return str(self.photo)

class Help(models.Model):
    question = models.CharField(max_length=500)
    answer = models.TextField()
    photo = models.ForeignKey(HelpPhoto, on_delete=models.CASCADE)
    def __str__(self):
        return self.question

class Footer(models.Model):
    logotype = models.FileField(upload_to='static/icons/footer')
    info = models.TextField()
    number = models.CharField(max_length=100)


class Advantages(models.Model):
    icon = models.FileField(upload_to='static/icons/advantages')
    title = models.CharField(max_length=100)
    description = models.TextField()
    def __str__(self):
        return self.title


class Slider(models.Model):
    photo = models.ImageField(upload_to="static/images/slider")
    link = models.CharField(max_length=1000, null=True, blank=True)


class PublicOffer(models.Model):
    title = models.CharField(max_length=100)
    description = RichTextField() #ckeditor
    def __str__(self):
        return self.title



contact_choice = [('phone', 'Phone Number'), ('email', 'E-mail'), 
('instagram', 'Instagram'), ('telegram', 'Telegram'), ('whatsapp', 'Whatsapp')]

class FooterInfo(models.Model):
    contact_type = models.CharField(choices=contact_choice, max_length=100)
    link = models.CharField(max_length=100)
    def save(self, *args, **kwargs):
        if self.contact_type == 'whatsapp':
            self.link = 'https://wa.me/' + self.link
        elif self.contact_type == 'phone':
            self.link =     self.link
        super(FooterInfo, self).save(*args, **kwargs)

    def __str__(self):
        return self.link    
        



list_of_models = [Collection, Item, News, About, Help, Footer, Advantages, Slider, PublicOffer]
