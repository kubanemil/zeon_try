from dataclasses import fields
from rest_framework import serializers

from store.models import *


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        exclude = ('last_login', 'is_superuser', 'last_name', 'first_name', 'email', 'is_active', 'date_joined', 'groups', 'user_permissions',)

class CollectionSerializer(serializers.ModelSerializer):
    class Meta:
        model = Collection
        fields = '__all__'

class ItemSerializer(serializers.ModelSerializer):
    class Meta:
        model = Item
        fields = '__all__'

class FavoriteSerializer(serializers.ModelSerializer):
    class Meta:
        model = Item
        fields = ('photo1', 'name', 'price', 'old_price', 'discount', 'min_size', 'max_size', 
        'color1', 'color2', 'color3', 'color4', 'color5', 'color6', 'color7', 'color8', 'id', 'is_favorite')

        read_only_fields = ('photo1', 'name', 'price', 'old_price', 'min_size', 'max_size', 
        'color1', 'color2', 'color3', 'color4', 'color5', 'color6', 'color7', 'color8', 'id',)

class CollectionItemSerializer(serializers.ModelSerializer):
    # items = FavoriteSerializer(many=True)
    class Meta:
        model = Item
        fields = ('id', 'name', 'collection')

class NewsSerializer(serializers.ModelSerializer):
    class Meta:
        model = News
        fields = '__all__'

class AboutSerializer(serializers.ModelSerializer):
    class Meta:
        model = About
        fields = '__all__'

class HelpSerializer(serializers.ModelSerializer):
    class Meta:
        model = Help
        fields = '__all__'

class FooterSerializer(serializers.ModelSerializer):
    class Meta:
        model = Footer
        fields = '__all__'

class FooterInfoSerializer(serializers.ModelSerializer):
    class Meta:
        model = FooterInfo
        fields = '__all__'

class PublicOfferSerializer(serializers.ModelSerializer):
    class Meta:
        model = PublicOffer
        fields = '__all__'



list_of_serializers = [CollectionSerializer, ItemSerializer, NewsSerializer, 
AboutSerializer, HelpSerializer, FooterSerializer, PublicOfferSerializer]