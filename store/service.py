from rest_framework.pagination import PageNumberPagination

class MainPagination(PageNumberPagination):
    page_size = 8
    max_page_size= 10

class SimiliarPagination(PageNumberPagination):
    page_size = 5
    max_page_size = 10

class FavoritePagination(PageNumberPagination):
    page_size = 12
    max_page_size = 20


class CollectionItemPagination(PageNumberPagination):
    page_size = 1
    max_page_size = 2