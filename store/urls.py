from django.contrib import admin
from django.urls import path, include
from . import views
from django.conf.urls.static import static
from django.conf import settings

urlpatterns = [
    path("", views.home, name='home'),
    path("api/collection", views.CollectionApi.as_view(), name='collection_api'),
    path("api/item", views.ItemApi.as_view(), name='item_api'),
    path("api/news", views.NewsApi.as_view(), name='news_api'),
    path("api/about", views.AboutApi.as_view(), name='about_api'),
    path("api/help", views.HelpApi.as_view(), name='help_api'),
    path("api/footer", views.FooterApi.as_view(), name='footer_api'),
    path("api/footerinfo", views.FooterInfoApi.as_view(), name='footerinfo_api'),
    path("api/publicoffer", views.PublicOfferApi.as_view(), name='publicoffer_api'),
    # path("api/favoriteitem", views.FavoriteItemApi.as_view(), name='favoriteitem_api'),
    # path("api/favoriteitem/create", views.CreateFavouriteItemApi.as_view(), name='favoriteitem_create_api'),
    # path("api/favoriteitem/change/<int:pk>", views.ChangeFavouriteItemApi.as_view(), name='favoriteitem_change_api'),
    path("api/", include('rest_framework.urls')),
    path("api/collection/create", views.CreateCollectionApi.as_view(), name='collection_create_api'),
    path("api/favorite/add_remove/<pk>", views.AddDeleteFavoriteApi.as_view()),
    path("api/favorite", views.FavoriteApi.as_view()),
    path("api/collection/<pk>/item", views.CollectionItemApi.as_view()),
]+ static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

