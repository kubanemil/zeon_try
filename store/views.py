from gc import collect
from django.shortcuts import render
from rest_framework.generics import ListAPIView, RetrieveAPIView, ListCreateAPIView, RetrieveUpdateDestroyAPIView, CreateAPIView, RetrieveUpdateAPIView
from .models import Collection, Item
from .serializer import *
from .service import *
# Create your views here.

def home(request):
    return render(request, "store/home.html", context={'username':request.user})

class CollectionApi(ListAPIView):
    queryset = Collection.objects.all()
    serializer_class = CollectionSerializer
    # pagination_class = MainPagination
        

class CreateCollectionApi(CreateAPIView):
    serializer_class = CollectionSerializer

class ItemApi(ListAPIView):
    queryset = Item.objects.all()
    serializer_class = ItemSerializer

class AddDeleteFavoriteApi(RetrieveUpdateAPIView):
    queryset = Item.objects.all()
    serializer_class = FavoriteSerializer
class FavoriteApi(ListAPIView):
    queryset = Item.objects.filter(is_favorite=True)
    serializer_class = FavoriteSerializer
    pagination_class = FavoritePagination

class NewsApi(ListAPIView):
    queryset = News.objects.all()
    serializer_class = NewsSerializer
    pagination_class = MainPagination

class AboutApi(ListAPIView):
    queryset = About.objects.all()
    serializer_class = AboutSerializer

class HelpApi(ListAPIView):
    queryset = Help.objects.all()
    serializer_class = HelpSerializer

class FooterApi(ListAPIView):
    queryset = Footer.objects.all()
    serializer_class = FooterSerializer

class FooterInfoApi(ListAPIView):
    queryset = FooterInfo.objects.all()
    serializer_class = FooterInfoSerializer

class PublicOfferApi(ListAPIView):
    queryset = PublicOffer.objects.all()
    serializer_class = PublicOfferSerializer


class CollectionItemApi(RetrieveAPIView):
    queryset = Item.objects.all()
    serializer_class = CollectionItemSerializer
    pagination_class = CollectionItemPagination

list_of_api = [CollectionApi, ItemApi, NewsApi, 
AboutApi, HelpApi, FooterApi, PublicOfferApi]